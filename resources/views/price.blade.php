<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CV. SAKATA DIGDAYA</title>
  <!-- Fonts -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
  <link rel="shortcut icon" href="{{URL::asset('images/logo.jpeg')}}">
  <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
</head>
<body>
  <header class="header">
    <nav class="navbar navbar-expand-lg navbar-dark bg-transparent fixed-top">
      <div class="container">
        <a class="navbar-brand" href="/">CV. SAKATA DIGDAYA</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item ">
              <a class="nav-link" href="/">Halaman Depan <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
              <a class="nav-link" href="/price">Cek Harga <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <div class="jumbotron jumbotron-custom">
    <div class="overlay"></div>
    <div class="container">
      <div class="check-price-box row">
        <div class="col-md-8 text-center text-lg-left">
          <h2 class="text-white font-weight-bold">Cek Harga Tracking All-in</h2>
          <div class="card border-0">
            <div class="card-header bg-primary  px-2">
              <div class="text-white p-2">
                <span>Tracking All-in</span>
              </div>
            </div>
            <div class="card-body bg-secondary p-2">
              <div class="row">
                <div class="col-md-6">
                  <div class="card p-2 bg-white">
                    <p class="mb-2">Jemput</p>
                    <div class="card-body p-0">
                      <input id="pac-input" class="form-control pickup-area" name="" placeholder="Lokasi Jemput" />
                      {{-- <textarea id="pac-input" class="form-control pickup-area" name="" placeholder="Lokasi Jemput"></textarea> --}}
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="card bg-white p-2">
                    <p class="mb-2">Tujuan</p>
                    <div class="card-body p-0">
                      <input id="tuj-input" class="form-control pickup-area" name="" placeholder="Lokasi Tujuan" />
                    </div>
                  </div>
                </div>
                <div class="col-md-6 mt-4">
                  <div class="card">
                    <div class="card-body p-2">
                      <div class="tabs-price">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#car" role="tab" aria-controls="car" aria-selected="true">Jenis Armada</a>
                          </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="car" role="tabpanel" aria-labelledby="home-tab">
                            <div class="input-group mt-3">
                              <select class="select-car custom-select" id="inputGroupSelect02">
                                <option selected>Pilih kendaraan</option>
                                @foreach ($cars as $val)
                                  <option value="{{$val->id}}">{{$val->name_cars}}</option>
                                @endforeach
                              </select>
                              <div class="input-group-append">
                                <label class="input-group-text" for="inputGroupSelect02">Pilih</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-3">
                <button class="btn btn-warning btn-sm price-check" disabled data-toggle="modal" data-target="#priceModal">Dapatkan harga</button>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 text-center text-lg-left mt-4 mt-lg-0">
          <h2 class="text-white font-weight-bold">Cek Harga Sewa armada</h2>
          <div class="card border-0">
            <div class="card-header bg-primary  px-2">
              <div class="text-white p-2">
                <span>Sewa Armada</span>
              </div>
            </div>
            <div class="card-body bg-secondary p-2">
              <div class="row">
                <div class="col-md-12 mt-4">
                  <div class="card">
                    <div class="card-body p-2">
                      <div class="tabs-price">
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="car" role="tabpanel" aria-labelledby="home-tab">
                            <div class="input-group">
                              <select class="booking-select custom-select" id="inputGroupSelect02">
                                <option selected>Pilih durasi</option>
                                <option value="12 Jam">Per 12 Jam</option>
                                <option value="24 Jam">Per 24 Jam</option>
                                <option value="Per Minggu">Per Minggu</option>
                                <option value="Per Bulan">Per Bulan</option>
                                <option value="Per Tahun">Per Tahun</option>
                              </select>
                              <div class="input-group-append">
                                <label class="input-group-text" for="inputGroupSelect02">Pilih</label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="mt-3">
                <a href="" target="_blank" class="link-wa">
                  <button class="btn btn-warning btn-sm booking">Booking Sekarang</button>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="our-partner">
    <div class="container">
      <div class="title-box">
        <h2 class="mb-4">ARMADA KAMI</h2>
        <span>Kami menyediakan berbagai macam armada dengan banyak pilihan,seperti Blindvan,pickup,Box,CDE (colt diesel Engkel),CDD (Colt Diesel Double) DLL,dapat disesuaikan dengan kebutuhan anda</span>
      </div>
      <div class="partner-carousel" style="margin-top: -50px">
      </div>
    </div>
  </div>
  <footer class="footer">
    <div class="row">
      <div class="col-md-6 col-sm-12 mb-4 mb-lg-0">
        <div class="d-flex">
          <div class="contact-img">
            <img src="{{URL::asset('images/mail.png')}}" alt="">
          </div>
          <div style="margin: 5px">
            <a class="text-white" href="mailto:admin@sakatatrans.co.id">admin@sakatatrans.co.id</a>
          </div>
        </div>
        <div class="d-flex">
          <div class="contact-img">
            <img src="{{URL::asset('images/phone.png')}}" alt="">
          </div>
          <div style="margin: 5px;">
            <a class="text-white" href="https://wa.me/+6281111122989?text=Hai,%20Saya%20ingin%20memesan%20jasa%20sakata%20trans">081-1111-22-989</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 mb-4 mb-lg-0 text-lg-left text-center">
        <h4>Alamat Perusahaan</h4>
        Jl. Dirgantara 1 No.12, Gempolsari, Kec. Bandung Kulon, Kota Bandung, Jawa Barat 402515
      </div>
    </div>
  </footer>

  {{-- MODALL --}}
  
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Detail Armada</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body detail-armada">
          <div class="text-center">
            <img class="armada-img" src="{{URL::asset('images/blind-van-2.png')}}"/>
            <h4 class="mt-4 name">Grandmax</h4>
          </div>
          <div>
            <p>Kubikasi: <strong class="cubic"></strong> CBM</p>
            <p>Tinggi: <strong class="height"></strong> CM</p>
            <p>Lebar: <strong class="width"></strong> CM</p>
            <p>Panjang: <strong class="payload"></strong> CM</p>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="priceModal" tabindex="-1" role="dialog" aria-labelledby="priceModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Detail Tarif</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="loader"></div>
          <div class="price-modal d-none">
            <table class="table w-100 table-bordered table-hover table-responsive border-0">
              <thead style="font-size: 12px">
                <tr>

                  <th width="25%">Lok. Jemput</th>
                  <th width="25%">Lok. Tujuan</th>
                  <th width="25%">Armada</th>
                  <th width="25%">Gambar armada</th>
                  <th width="25%">Jarak</th>
                  <th width="25%">Harga</th>
                  
                </tr>
              </thead>
              <tbody style="font-size: 12px">
                <tr>
                  <td class="pickup">Padalarang</td>
                  <td class="destination">Bandung</td>
                  <td class="car-name">
                    Hino RK
                  </td>
                  <td>
                    <img class="img-car" width="85px" src="{{URL::asset('images/blind-van-2.png')}}"/>
                  </td>
                  <td><span class="total-distance">0</span></td>
                  <td><span class="total-price">0</span></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="modal-footer justify-content-center">
          <a href="" target="_blank" class="link-waCek">
            <button class="btn w-100 btn-primary btn-pesan d-none cekHarga">Pesan Sekarang Untuk nego Harga<img width="35px" src="{{URL::asset('images/wa-icon.png')}}"/></button>
          </a>
        </div>
      </div>
    </div>
  </div>

</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
{{-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script> --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js" integrity="sha512-4bp4fE4hv0i/1jLM7d+gXDaCAhnXXfGBKdHrBcpGBgnz7OlFMjUgVH4kwB85YdumZrZyryaTLnqGKlbmBatCpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script type="text/javascript"
        src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false&key=AIzaSyATaPvVhkGWXOPSS3baO5J3GMBtaIcshdA">
</script>
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script>
    var appUrl ="{{env('APP_URL')}}";
    // $("#pac-input")
    //   .geocomplete()
    //   .bind("geocode:result", function(event, result){
    //     console.log(result);
    //     getLatlong(result.name)
    // });

    // $("#destination-input")
    //   .geocomplete()
    //   .bind("geocode:result", function(event, result){
    //     console.log(result);
    // });

    
    // function getLatlong(address){
    //   var geocoder = new google.maps.Geocoder();
    //   geocoder.geocode({ 'address': address }, function (results, status) {
    //     if (status == google.maps.GeocoderStatus.OK) {
    //       var latitude = results[0].geometry.location.latitude;
    //       var longitude = results[0].geometry.location.longitude;
    //       console.log('address', results)
    //     }
    //   });
    // }

   
</script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
</html>
