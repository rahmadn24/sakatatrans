@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ ('./vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ ('./vendor/izitoast/izitoast.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/styles/css/custom.css') }}">
	<link rel="stylesheet" href="{{ asset('./plugins/bootstrap-4.5.2/css/bootstrap-toggle.min.css') }}">

@stop

@section('title', 'Sakatatrans | Price Cars')

@section('content_header')
<h1>Price Cars</h1>
@stop

@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h5 class="my-auto font-weight-bold text-primary float-left">Price Cars Table</h5>
        <button type="button" name="create_record" id="create_record" class="btn btn-primary btn-sm float-right round" style=" float: right;"><i class="fas fa-plus-circle"></i> Add Price Cars</button>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="gift" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Price</th>
                        <th>Cars</th>
                        <th>Level</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
		</div>
	</div>
</div>

<div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Price Cars</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span id="form_result"></span>
                    <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf

                        <div class="input-group mb-3">
                            <label for="price" class="col-md-3 col-form-label text-right">
								Price<span class="requiredFieldMark">*</span> :
							</label>
                             <div class="input-group-append">
                                <span class="input-group-text">Rp.</span>
                            </div>
                            <input type="text" data-mask="00000000000" class="form-control" name="price" id="price" placeholder="100.000" value="">
                        </div>

                       <div class="form-group row">
							<label for="voucher" class="col-md-3 col-form-label text-right">
								Cars<span class="requiredFieldMark">*</span> :
							</label>
							<div class="col-md-9">
								<select name="cars" class="form-control round" id="cars" required>
									<option value="">-- Select Cars --</option>
									@foreach ($cars as $car)
										<option value="{{$car->id}}">{{$car->name_cars}}</option>
									@endforeach
								</select>
							</div>
						</div>

                         <div class="form-group row" id="fieldLevel">
							<label for="level" class="col-md-3 col-form-label text-right">
								Level<span class="requiredFieldMark">*</span> :
							</label>
							<div class="col-md-9">

                                <select id="select-state" name="level" id="level" class="form-control" required placeholder="Pick a state...">
								{{-- <select name="level" id="level" class="form-control round" required> --}}
									<option value="">-- Select Level --</option>
									<option value="1">Under 10 Km</option>
									<option value="2">Under 20 Km</option>
									<option value="3">Under 30 Km</option>
									<option value="4">Under 40 Km</option>
									<option value="5">Under 50 Km</option>
									<option value="6">Under 60 Km</option>
									<option value="7">Under 70 Km</option>
									<option value="8">Under 80 Km</option>
									<option value="9">Under 90 Km</option>
									<option value="10">Under 100 Km</option>
									<option value="11">Under 120 Km</option>
									<option value="12">Under 140 Km</option>
									<option value="13">Under 160 Km</option>
									<option value="14">Under 180 Km</option>
									<option value="15">Under 200 Km</option>
									<option value="16">Under 220 Km</option>
									<option value="17">Under 240 Km</option>
									<option value="18">Under 260 Km</option>
									<option value="19">Under 280 Km</option>
									<option value="20">Under 300 Km</option>
									<option value="21">Under 320 Km</option>
									<option value="22">Under 340 Km</option>
									<option value="23">Under 360 Km</option>
									<option value="24">Under 380 Km</option>
									<option value="25">Under 400 Km</option>
									<option value="26">Under 450 Km</option>
									<option value="27">Under 500 Km</option>
									<option value="28">Under 550 Km</option>
									<option value="29">Under 600 Km</option>
									<option value="30">Under 650 Km</option>
									<option value="31">Under 700 Km</option>
									<option value="32">Under 750 Km</option>
									<option value="33">Under 800 Km</option>
									<option value="34">Under 850 Km</option>
									<option value="35">Under 900 Km</option>
									<option value="36">Under 950 Km</option>
									<option value="37">Under 1000 Km</option>
								</select>
							</div>
						</div>

                        <br/>
                        <div class="form-group" align="center">
                            <input type="hidden" name="action" id="action" />
                            <input type="hidden" name="hidden_id" id="hidden_id" />
                            <input type="submit" name="action_button" id="action_button" class="btn btn-primary" value="Add" />
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Confirmation</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{ ('./vendor/jquery/jquery.mask.min.js') }}"></script>
    <script src="{{ ('./vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ ('./vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ ('./vendor/izitoast/izitoast.min.js') }}"></script>
	<script src="{{ ('./vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
	<script src="{{ ('./vendor/bootstrap/js/bootstrap-toggle.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css" integrity="sha256-ze/OEYGcFbPRmvCnrSeKbRTtjG4vGLHXgOqsyLFTRjg=" crossorigin="anonymous" />
    <script>
        var tableLink     = "{{ url('managementSakata-price') }}";
        var postPriceCars      = "{{ url('price-store') }}";
        var priceCarsUpdate    = "{{ url('price/update') }}";
    </script>
    <script src="{{ asset('assets/styles/js/price-cars.js') }}"></script>
@stop