@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ ('./vendor/datatables/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ ('./vendor/izitoast/izitoast.min.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/styles/css/custom.css') }}">
	<link rel="stylesheet" href="{{ asset('./plugins/bootstrap-4.5.2/css/bootstrap-toggle.min.css') }}">

@stop

@section('title', 'Sakatatrans | Cars')

@section('content_header')
<h1>Cars</h1>
@stop

@section('content')
<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h5 class="my-auto font-weight-bold text-primary float-left">Cars Table</h5>
        <button type="button" name="create_record" id="create_record" class="btn btn-primary btn-sm float-right round" style=" float: right;"><i class="fas fa-plus-circle"></i> Add Cars</button>
	</div>
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="gift" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Image</th>
                        <th>Name Cars</th>
                        <th>Long</th>
                        <th>Cubic</th>
                        <th>Width</th>
                        <th>Height</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
		</div>
	</div>
</div>

<div id="formModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Gift</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <span id="form_result"></span>
                    <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="image" class="col-md-3 col-form-label text-right">
                                Image<span class="requiredFieldMark">*</span> :
                            </label>
                            <div class="col-md-9">
                                <div id="addPreviewContainer" class="addPreviewContainer">
                                    <img id="addProductThumb" alt="" src="{{ ('images/placeholder.png') }}" data-src="{{ ('images/placeholder.png')}}" class="img-fluid img-thumbnail previewAdd"/>
                                </div>
                                <div class="custom-file mt-2">
                                    <input type="hidden" name="oldImage" id="oldImage"  onchange="changeLabelFile(this); previewimg(this)">
                                    <input type="hidden" name="copyImage" id="copyImage">
                                    <input type="file" name="image" class="custom-file-input round" id="image" accept=".jpg, .png, .gif" onchange="changeLabelFile(this); previewimg(this); imageClone(this)">
                                    <label class="custom-file-label" id="imgLabel">Choose file...</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-md-3 col-form-label text-right">
                                Name Cars<span class="requiredFieldMark">*</span> :
                            </label>
                            <div class="col-md-9 ">
                               <input type="text" class="form-control" name="name_cars" id="name-cars" placeholder="your car" value="">
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <label for="payload" class="col-md-3 col-form-label text-right">
								Long<span class="requiredFieldMark">*</span> :
							</label>
                            <input type="text" data-mask="0000000000000" class="form-control" name="maximum_payload" placeholder="10" id="maximum-payload" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">Cm</span>
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <label for="cubic" class="col-md-3 col-form-label text-right">
								Cubic<span class="requiredFieldMark">*</span> :
							</label>
                            <input type="text" data-mask="0000000000000" class="form-control" name="cubic" placeholder="20" id="cubic" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">Cbm</span>
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <label for="width" class="col-md-3 col-form-label text-right">
								Width<span class="requiredFieldMark">*</span> :
							</label>
                            <input type="text" data-mask="0000000000000" class="form-control" name="width" placeholder="20" id="width" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">Cm</span>
                            </div>
                        </div>

                        <div class="input-group mb-3">
                            <label for="height" class="col-md-3 col-form-label text-right">
								Height<span class="requiredFieldMark">*</span> :
							</label>
                            <input type="text" data-mask="0000000000000" class="form-control" name="height" placeholder="30" id="height" value="">
                            <div class="input-group-append">
                                <span class="input-group-text">Cm</span>
                            </div>
                        </div>

                        <br/>
                        <div class="form-group" align="center">
                            <input type="hidden" name="action" id="action" />
                            <input type="hidden" name="hidden_id" id="hidden_id" />
                            <input type="submit" name="action_button" id="action_button" class="btn btn-primary" value="Add" />
                        </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title">Confirmation</h2>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script src="{{ ('./vendor/jquery/jquery.mask.min.js') }}"></script>
    <script src="{{ ('./vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ ('./vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ ('./vendor/izitoast/izitoast.min.js') }}"></script>
	<script src="{{ ('./vendor/sweetalert2/sweetalert2.all.min.js') }}"></script>
	<script src="{{ ('./vendor/bootstrap/js/bootstrap-toggle.min.js') }}"></script>
    <script>
        var tableLink     = "{{ url('managementSakata') }}";
        var defaultSrc    = "{{ ('images/placeholder.png') }}";
        var postCars      = "{{ url('cars-store') }}";
        var carUpdate    = "{{ url('cars/update') }}";
    </script>
    <script src="{{ asset('assets/styles/js/cars.js') }}"></script>
@stop