<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>CV. SAKATA DIGDAYA</title>
  <!-- Fonts -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ URL::asset('css/app.css') }}">
  <link rel="shortcut icon" href="{{URL::asset('images/logo.jpeg')}}">
</head>
<body>
  <header class="header">
    <nav class="navbar navbar-expand-lg navbar-dark bg-transparent fixed-top">
      <div class="container">
        <a class="navbar-brand" href="#">CV. aLIM RUGI</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="/">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/price">Cek Harga <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </header>
  <div class="jumbotron jumbotron-custom homepage">
    <div class="overlay homepage"></div>
    <div class="container position-relative">
      <div class="jumbotron-text-box text-white">
        <h2>Sakata Trans</h2>   
        <small>Besar harapan kami ,sakata trans bisa menjadi solusi pengiriman barang dan dapat menjalin kemitraan dengan baik</small> 
        <div class="mt-4">
          <a href="/price">
            <button class="btn btn-sm btn-primary">Cek Harga Sekarang</button>
          </a>
        </div>
      </div>
    </div>
  </div>
  <div class="about">
    <div class="container">
      <div class="about-content">
        <div class="img-box">
          <img class="about-img" src="{{URL::asset('images/truck.png')}}">
        </div>
        <div class="mb-5">
          <h3 class="font-weight-normal mb-4">TENTANG SAKATA TRANS</h3>
          <span class="text-muted">Kami perusahaan yang bergerak dibidang transportasi darat,yang berdomisili di kota bandung,
            siap melayani pengiriman barang keberbagai tujuan dengan cepat,tepat,aman dan dapat diandalkan, 
            kepuasan pelanggan adalah tujuan kami.</span>
        </div>
        <div>
          <span class="text-muted">Serta service area yang menjangkau meliputi pulau</span>
          <div class="mt-3">
            <h5 style="letter-spacing: 10px">JAWA & BALI.</h5>
            <a href="https://wa.me/+6281111122989?text=Hai,%20Saya%20ingin%20memesan%20jasa%20sakata%20trans">
              <button class="btn btn-outline-primary mt-4">Pesan Sekarang</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="service">
    <div class="container" style="margin-top: -50px;">
      <div class="row text-center">
        <div class="col-md-4 col-sm-12 mb-4">
          <div class="card-shadow">
            <div class="badge-circle">
              <img src="{{URL::asset('images/staff.svg')}}" />
            </div>
            <h4>Profesional Staff</h4>
            <p>Kami memiliki staff yang profesional dibidangnya yang akan membantu semua kebutuhan anda dari mulai admin, customer service, dan driver-driver yang berpengalaman</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 mb-4">
          <div class="card-shadow">
            <div class="badge-circle">
              <img src="{{URL::asset('images/shipping.svg')}}" />
            </div>
            <h4>Sewa Armada</h4>
            <p>Kami menyediakan layanan sewa armada berdasarkan lama pemakaian dengan detail 12 jam,24 jam,mingguan,bulanan hingga tahunan</p>
          </div>
        </div>
        <div class="col-md-4 col-sm-12 mb-4">
          <div class="card-shadow">
            <div class="badge-circle">
              <img src="{{URL::asset('images/shipping.svg')}}" />
            </div>
            <h4>Layanan</h4>
            <p>Pengiriman tracking secara Door to Door antara lain pindahan rumah,kantor,kostan,pengiriman sembako,pengiriman home industri,pengiriman textile dll,pelayanan 24 jam</p>
          </div>  
        </div>
      </div>
    </div>
  </div>
  <div class="our-partner">
    <div class="container">
      <div class="title-box">
        <h2 class="mb-4">ARMADA KAMI</h2>
        <span>Kami menyediakan berbagai macam armada dengan banyak pilihan,seperti Blindvan,pickup,Box,CDE (colt diesel Engkel),CDD (Colt Diesel Double) DLL,dapat disesuaikan dengan kebutuhan anda</span>
      </div>
      <div class="partner-carousel" style="margin-top: -50px;">
      </div>
    </div>
  </div>
  <div class="maps">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.683463398019!2d107.55051801414487!3d-6.928387169740641!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e5f4e8293c11%3A0xc8af513c33724a33!2sSakata%20Trans!5e0!3m2!1sid!2sid!4v1605889786302!5m2!1sid!2sid" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
  </div>
  <footer class="footer">
    <div class="row">
      <div class="col-md-6 col-sm-12 mb-4 mb-lg-0">
        <div class="d-flex">
          <div class="contact-img">
            <img src="{{URL::asset('images/mail.png')}}" alt="">
          </div>
          <div style="margin: 5px">
            <a class="text-white" href="mailto:admin@sakatatrans.co.id">admin@sakatatrans.co.id</a>
          </div>
        </div>
        <div class="d-flex">
          <div class="contact-img">
            <img src="{{URL::asset('images/phone.png')}}" alt="">
          </div>
          <div style="margin: 5px;">
            <a class="text-white" href="https://wa.me/+6281111122989?text=Hai,%20Saya%20ingin%20memesan%20jasa%20sakata%20trans">081-1111-22-989</a>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-sm-12 mb-4 mb-lg-0 text-left">
        <h4>Alamat Perusahaan</h4>
        Jl. Dirgantara 1 No.12, Gempolsari, Kec. Bandung Kulon, Kota Bandung, Jawa Barat 402515
      </div>
    </div>
  </footer>

  {{-- MODALL --}}
  
  <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLongTitle">Detail Armada</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body detail-armada">
          <div class="text-center">
            <img class="armada-img" style="width: 100% !important" src="{{URL::asset('images/blind-van-2.png')}}"/>
            <h4 class="mt-4 name">Grandmax</h4>
          </div>
          <div>
            <p>Kubikasi: <strong class="cubic"></strong> CBM</p>
            <p>Tinggi: <strong class="height"></strong> CM</p>
            <p>Lebar: <strong class="width"></strong> CM</p>
            <p>Panjang: <strong class="payload"></strong> CM</p>
          </div>
        </div>
        <div class="modal-footer">
          <a href="/price">
            <button class="btn btn-sm btn-primary">Cek Harga Sekarang</button>
          </a>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

</body>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script>
    var appUrl ="{{env('APP_URL')}}";
</script>
<script src="{{ URL::asset('assets/js/app.js') }}"></script>
</html>
