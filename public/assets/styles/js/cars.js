var table = $('#gift').DataTable({
    processing: true,
    serverSide: true,
    ajax: tableLink,
    columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'image', name: 'image'},
        {data: 'name_cars', name: 'name_cars'},
        {data: 'maximum_payload', name: 'maximum_payload'},
        {data: 'cubic', name: 'cubic'},
        {data: 'width', name: 'width'},
        {data: 'height', name: 'height'},
        {data: 'action', name: 'action', orderable: false, searchable: false},
    ],
});

$('#create_record').click(function(){
    $('.modal-title').text("Add Cars");
    $('#action_button').val("Submit");
    $('#action').val("Add");
    $('#formModal').modal('show');
});

$(document).ready(function (){
    $('#status')
        .prop('checked', false)
        .parent()
        .removeClass('btn-primary')
        .addClass(['btn-light', 'off']);
});
const changeLabelFile = (e) => {
    var fileName = $(e).val().split('\\')[2];
    var fileExt  = $(e).val().split('.').pop();
    if(fileName) {
        if (fileName.length > 30) {
                fileName = fileName.substring(0, 30) + '...' + fileExt;
        }
    }
    $('#imgLabel').html(fileName);
}

function previewimg(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
                $('#addProductThumb').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function imageClone(e) {
    var $this = $(e);
    if($this.val()) {
        var findCloned = $(document).find('#' + e.id + '-clone');
        if(findCloned) {
            findCloned.remove();
            var $thisClone = $this.clone().insertAfter($this);
            $thisClone.prop({
                id: e.id + '-clone',
                required: false
            }).hide();
        } else{
            var $thisClone = $this.clone().insertAfter($this);
            $thisClone.prop({
                id: e.id + '-clone',
                required: false
            }).hide();
        }
} else {
        $this.remove();
        var findCloned = $(document).find('#' + e.id + '-clone');
        findCloned.prop({
            id: e.id,
            disabled: false,
            required: true
        }).show();
    }
}

function clearAddForm() {
    $('#sample_form')[0].reset();
}

$('#formModal').on('hidden.bs.modal', () => {
    clearAddForm();
})

$('#sample_form').on('submit', function(event){
    event.preventDefault();
    if($('#action').val() == 'Add')
    {
        $.ajax({
            url: postCars,
            method:"POST",
            data:new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            beforeSend: function(){
                $('#loader-container').removeClass('d-none');
            },
            success:function(data)
            {
                $('#loader-container').addClass('d-none');
                $('#formModal').modal('hide');
                clearAddForm();
                if(data) {
                    iziToast.success({
                        title: 'Success',
                        message: data.message,
                        position: 'topRight'
                    });
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: data.message,
                        position: 'topRight'
                    });
                }
                $('#sample_form')[0].reset();
                table.ajax.reload()
            },

            error: (xhr, ajaxOptions, thrownError) => {
                $('#loader-container').addClass('d-none');
                var image               = (xhr.responseJSON.errors.image) ? xhr.responseJSON.errors.image + '<br/>' : '';
                var name_cars           = (xhr.responseJSON.errors.name_cars) ? xhr.responseJSON.errors.name_cars + '<br/>' : '';
                var maximum_payload     = (xhr.responseJSON.errors.maximum_payload) ? xhr.responseJSON.errors.maximum_payload + '<br/>' : '';
                var cubic               = (xhr.responseJSON.errors.cubic) ? xhr.responseJSON.errors.cubic + '<br/>' : '';
                var width               = (xhr.responseJSON.errors.width) ? xhr.responseJSON.errors.width + '<br/>' : '';
                var height              = (xhr.responseJSON.errors.height) ? xhr.responseJSON.errors.height + '<br/>' : '';


                iziToast.error({
                    title: 'Error',
                    displayMode: 2,
                    message:  [
                        image + name_cars + maximum_payload + cubic + width + height
                        ],
                    position: 'topRight',
                    layout: 2
                });
            }
        });
    }

    if($('#action').val() == "Edit")
    {
        $.ajax({
            url: carUpdate,
            method:"POST",
            data:new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            beforeSend: function(){
                $('#loader-container').removeClass('d-none');
            },
            success: function (response) {
                $('#loader-container').addClass('d-none');
                $('#formModal').modal('hide');
                if (response.status === 'success') {
                    iziToast.success({
                        title: 'Success',
                        message: response.message,
                        position: 'topRight'
                    });
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: response.message,
                        position: 'topRight'
                    });
                }
                $('#sample_form')[0].reset();
                table.ajax.reload()
            },

            error: (xhr, ajaxOptions, thrownError) => {
                $('#loader-container').addClass('d-none');
                var image               = (xhr.responseJSON.errors.image) ? xhr.responseJSON.errors.image + '<br/>' : '';
                var name_cars           = (xhr.responseJSON.errors.name_cars) ? xhr.responseJSON.errors.name_cars + '<br/>' : '';
                var maximum_payload     = (xhr.responseJSON.errors.maximum_payload) ? xhr.responseJSON.errors.maximum_payload + '<br/>' : '';
                var cubic               = (xhr.responseJSON.errors.cubic) ? xhr.responseJSON.errors.cubic + '<br/>' : '';
                var width               = (xhr.responseJSON.errors.width) ? xhr.responseJSON.errors.width + '<br/>' : '';
                var height              = (xhr.responseJSON.errors.height) ? xhr.responseJSON.errors.height + '<br/>' : '';


                iziToast.error({
                    title: 'Error',
                    displayMode: 2,
                    message:  [
                        image + name_cars + maximum_payload + cubic + width + height
                        ],
                    position: 'topRight',
                    layout: 2
                });
            }
        });
    }
});

$(document).on('click', '.edit', function(){
    var carId = $(this).attr('carId');
    $('#form_result').html('');
    $.ajax({
        url:"/cars/"+carId+"/edit",
        dataType:"json",
        success:function(html){
            $('#oldImage').val(html.data.image);
            $('#addProductThumb').prop('src', html.data.image_do);
            $('#name-cars').val(html.data.name_cars);
            $('#maximum-payload').val(html.data.maximum_payload);
            $('#cubic').val(html.data.cubic);
            $('#height').val(html.data.height);
            $('#width').val(html.data.width);
            $('#hidden_id').val(html.data.id);
            $('.modal-title').text("Edit Cars");
            $('#action_button').val("Update Data");
            $('#action').val("Edit");
            $('#formModal').modal('show');
        }
    })
});

deleteHandler = (e) => {
	Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, delete it!'
	}).then(function(result) {
		if (result.value) {
			$.ajax({
				type: "GET",
				url: $(e).attr('href'),
				success: function (response) {
					if (response.status === 'success') {
						iziToast.success({
							title: 'Success',
							message: response.message,
							position: 'topRight'
						});
					} else {
						iziToast.error({
							title: 'Error',
							message: response.message,
							position: 'topRight'
						});
					}
					table.ajax.reload()
				}
			});
		} else {}
	})
}

$("#formModal").on('hide.bs.modal', function() {
    $('#sample_form')[0].reset();
    $('#imgLabel').html('Choose File...');
    $('.previewAdd').prop('src', defaultSrc);
});
