var table = $('#gift').DataTable({
    processing: true,
    serverSide: true,
    ajax: tableLink,
    columns: [
        {data: 'DT_RowIndex', name: 'DT_RowIndex'},
        {data: 'price', name: 'price'},
        {data: 'cars_id', name: 'cars_id'},
        {data: 'level', name: 'level'},
        {data: 'action', name: 'action', orderable: false, searchable: false},
    ],
});

$('#create_record').click(function(){
    $('.modal-title').text("Add Price Cars");
    $('#action_button').val("Submit");
    $('#action').val("Add");
    $('#formModal').modal('show');
});

function clearAddForm() {
    $('#sample_form')[0].reset();
}

$('#formModal').on('hidden.bs.modal', () => {
    clearAddForm();
})

$('#sample_form').on('submit', function(event){
    event.preventDefault();
    if($('#action').val() == 'Add')
    {
        $.ajax({
            url: postPriceCars,
            method:"POST",
            data:new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            beforeSend: function(){
                $('#loader-container').removeClass('d-none');
            },
            success:function(data)
            {
                $('#loader-container').addClass('d-none');
                $('#formModal').modal('hide');
                clearAddForm();
                if(data) {
                    iziToast.success({
                        title: 'Success',
                        message: data.message,
                        position: 'topRight'
                    });
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: data.message,
                        position: 'topRight'
                    });
                }
                $('#sample_form')[0].reset();
                table.ajax.reload()
            },

            error: (xhr, ajaxOptions, thrownError) => {
                $('#loader-container').addClass('d-none');
                var price               = (xhr.responseJSON.errors.price) ? xhr.responseJSON.errors.price + '<br/>' : '';
                iziToast.error({
                    title: 'Error',
                    displayMode: 2,
                    message:  [
                        price
                        ],
                    position: 'topRight',
                    layout: 2
                });
            }
        });
    }

    if($('#action').val() == "Edit")
    {
        $.ajax({
            url: priceCarsUpdate,
            method:"POST",
            data:new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType:"json",
            beforeSend: function(){
                $('#loader-container').removeClass('d-none');
            },
            success: function (response) {
                $('#loader-container').addClass('d-none');
                $('#formModal').modal('hide');
                if (response.status === 'success') {
                    iziToast.success({
                        title: 'Success',
                        message: response.message,
                        position: 'topRight'
                    });
                } else {
                    iziToast.error({
                        title: 'Error',
                        message: response.message,
                        position: 'topRight'
                    });
                }
                $('#sample_form')[0].reset();
                table.ajax.reload()
            },

            error: (xhr, ajaxOptions, thrownError) => {
                $('#loader-container').addClass('d-none');
                var price               = (xhr.responseJSON.errors.price) ? xhr.responseJSON.errors.price + '<br/>' : '';

                iziToast.error({
                    title: 'Error',
                    displayMode: 2,
                    message:  [
                        price
                        ],
                    position: 'topRight',
                    layout: 2
                });
            }
        });
    }
});

$(document).on('click', '.edit', function(){
    var priceId = $(this).attr('priceId');
    $('#form_result').html('');
    $.ajax({
        url:"/price/"+priceId+"/edit",
        dataType:"json",
        success:function(html){
            $('#cars').val(html.data.cars_id).trigger('change');
            $('#level').val(html.data.level).trigger('change');
            $('#price').val(html.idrPrice).mask('000.000.000', {reverse: true});
            $('#hidden_id').val(html.data.id);
            $('.modal-title').text("Edit Price Cars");
            $('#action_button').val("Update Data");
            $('#action').val("Edit");
            $('#formModal').modal('show');
        }
    })
});

deleteHandler = (e) => {
	Swal.fire({
		title: 'Are you sure?',
		text: "You won't be able to revert this!",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, delete it!'
	}).then(function(result) {
		if (result.value) {
			$.ajax({
				type: "GET",
				url: $(e).attr('href'),
				success: function (response) {
					if (response.status === 'success') {
						iziToast.success({
							title: 'Success',
							message: response.message,
							position: 'topRight'
						});
					} else {
						iziToast.error({
							title: 'Error',
							message: response.message,
							position: 'topRight'
						});
					}
					table.ajax.reload()
				}
			});
		} else {}
	})
}

$("#formModal").on('hide.bs.modal', function() {
    $('#sample_form')[0].reset();
});

$(document).ready(function (){
    $( '#price' ).mask('000.000.000', {reverse: true});
});

//search input select
$(document).ready(function () {
    $('select').selectize({
        sortField: 'text'
    });
});