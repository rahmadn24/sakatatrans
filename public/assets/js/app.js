$(document).ready(function () {
  function slick() {
    $('.partner-carousel').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 1500,
      responsive: [
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
      ]
    });
  }
  $('.slick-arrow').hide()
  $(window).scroll(function () {
    if ($(this).scrollTop() >= 100) {
      $('.navbar').removeClass('bg-transparent')
      $('.navbar-brand').addClass('text-dark')
      $('.nav-link').addClass('text-dark')
      $('.navbar').addClass('bg-white')
    } else {
      $('.navbar').addClass('bg-transparent')
      $('.navbar-brand').removeClass('text-dark')
      $('.nav-link').removeClass('text-dark')
      $('.navbar').removeClass('bg-white')
    }
  })

  var distance;
  var cars_id;
  var res;
  var lat1;
  var long1;
  var lat2;
  var long2;
  var from;
  var to;
  $('.select-car').on('change', function () {
    $('.btn.price-check').removeAttr('disabled')
    cars_id = $(this).val()
  })
  function initialize() {
    var address = (document.getElementById('pac-input'));
    var autocomplete = new google.maps.places.Autocomplete(address);
    
    autocomplete.setTypes(['geocode']);
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        return;
      }
      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }
      var latitude = place.geometry.location.lat();
      var longitude = place.geometry.location.lng();
      lat1 = latitude;
      long1 = longitude;
      from = place.formatted_address
    });
  }

  function initialize1() {
    var address1 = (document.getElementById('tuj-input'));
    var autocomplete1 = new google.maps.places.Autocomplete(address1);
    autocomplete1.setTypes(['geocode']);

    google.maps.event.addListener(autocomplete1, 'place_changed', function() {
        var place = autocomplete1.getPlace();
        if (!place.geometry) {
            return;
        }
    if (place.address_components) {
        address = [
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
    }
      var latitude = place.geometry.location.lat();
      var longitude = place.geometry.location.lng();
      lat2 = latitude;
      long2 = longitude
      to = place.formatted_address
    });
  }

  google.maps.event.addDomListener(window, 'load', initialize);
  google.maps.event.addDomListener(window, 'load', initialize1);
    
  $('.price-check').click(function () {
    calcCrow(lat1, long1, lat2, long2)
    var objParam = {
      distance: distance,
      id: cars_id
    };
    $.ajax({
      async: false,
      type: "POST",
      url: appUrl + 'api/getDetail',
      xhrFields: {
        withCredentials: false
      },
      data: {
        json: JSON.stringify(objParam)
      },
      success: function (response) {
        $('.modal-body .loader').addClass('d-none')
        $('.modal-body .price-modal').removeClass('d-none')
        $('.btn-pesan').removeClass('d-none')
        res = response.data
        $('td.car-name').text(res.cars.name_cars)
        $('.total-price').text(_formatCurrency(res.totalPrice))
        $('.img-car').attr('src', res.image)
        $('td.pickup').text(from)
        $('td.destination').text(to)
      },
      error: function (error) {
        console.log("ERROR:", error);
      },
    })
  })

  var _formatCurrency = function (amount) {
    return "Rp." + parseFloat(amount).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
  };

  $('#priceModal').on('hidden.bs.modal', function () {
    // do something…
    $('.modal-body .loader').removeClass('d-none')
    $('.modal-body .price-modal').addClass('d-none')
    $('.btn-pesan').addClass('d-none')
  })


  function calcCrow(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = toRad(lat2 - lat1);
    var dLon = toRad(lon2 - lon1);
    var lat1 = toRad(lat1);
    var lat2 = toRad(lat2);
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    distance = R * c;
    $('#priceModal .total-distance').text(distance.toFixed(1) + ' KM');
  }

  // Converts numeric degrees to radians
  function toRad(value) {
    return value * Math.PI / 180;
  }

  var datas;
  $.ajax({
    type: "GET",
    url: appUrl + "api/getCars",
    // data: formdata,
    success: function (data) {
      datas = data.data;
      detailArmada(datas)
    },
    error: function (error) {
      console.log('error')
    },
    dataType: "json"
  });

  function detailArmada(datas) {
    if(datas){
      datas.forEach(function (val) {
        $(".partner-carousel").append(`
        <div class="partner-box">
          <img style="width: 250px; object-fit: contain; height: 245px" src="${val.image}"/>
          <h4>${val.cars_name}</h4>
          <button type="button" class="btn btn-primary detail-modal" id=${val.id} data-toggle="modal" data-target="#exampleModalCenter">
            Detail armada
          </button>
        </div>
        `);
      });
    }
    slick()
  }
  $('body').on('hidden.bs.modal', '#exampleModalCenter', function () {
    $('.detail-armada .cubic').text('')
    $('.detail-armada .height').text('')
    $('.detail-armada .price').text('')
    $('.detail-armada .payload').text('')
    $('.detail-armada .width').text('')
    $('.detail-armada .name').text('')
    $('.detail-armada .name').text('')
    $('.detail-armada .armada-img').attr('src', '')
  });
  $(".partner-carousel").on("click", ".detail-modal", function () {
    var id = $(this).attr('id')
    var filter = datas.filter(function (val) {
      return val.id == id
    })
    filter.forEach(function (value) {
      $('.detail-armada .cubic').text(value.cubic)
      $('.detail-armada .height').text(value.height)
      $('.detail-armada .price').text(value.price)
      $('.detail-armada .payload').text(value.maximum_payload)
      $('.detail-armada .width').text(value.width)
      $('.detail-armada .armada-img').attr('src', value.image)
      $('.detail-armada .name').text(value.cars_name)
    })
  });
  var valBook;
  $('.booking-select').on('change', function () {
    valBook = $(this).val()
  })

  $('.btn.booking').click(function (event) {
    var text = `https://api.whatsapp.com/send?phone=+6281111122989&text=Halo Sakatatrans,%0aSaya ingin memesan kendaraan untuk jangka waktu ${valBook}.%0a`
    $('a.link-wa').attr('href', text)
  })

  $('.btn.cekHarga').on('click', function (event) {
    var pickup = $('textarea.pickup-area').val()
    var dest = $('textarea.destination').val();
    var text = `https://api.whatsapp.com/send?phone=+6281111122989&text=Halo Sakatatrans, Saya ingin memesan kendaraan dengan detail %0aNama Kendaraan : ${res.cars.name_cars}
    %0aAlamat Jemput : ${pickup}%0aAlamat Tujuan : ${dest} %0a`
    $('a.link-waCek').attr('href', text)
  })

})