<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->integer('price')->nullable();
            $table->bigInteger('cars_id')->unsigned()->nullable();
            $table->foreign('cars_id')->references('id')->on('cars')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('level')->comment('1: under 30km, 2: under 60km, 3: under 90km, 4: under 120km, 5: 120 up');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
