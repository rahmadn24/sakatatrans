<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();
            $table->string('name_cars')->nullable();
            $table->longText('image')->nullable();
            $table->integer('maximum_payload')->nullable();
            $table->integer('cubic')->nullable();
            $table->integer('width')->nullable()->comment('centimeters');
            $table->integer('height')->nullable()->comment('centimeters');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
