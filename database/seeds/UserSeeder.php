<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{

    public function __construct()
    {
        $this->model = new User;
    }

    public function run()
    {
        $this->model->truncate();

        $this->model->create([
            'name' => 'Super admin',
            'email' => 'superadmin@sakatatrans.co.id',
            'password' => Hash::make('p@ssw0rd'),
        ]);

        $this->model->create([
            'name' => 'Admin',
            'email' => 'admin@sakatatrans.co.id',
            'password' => Hash::make('sakatatrans123'),
        ]);
    }
}
