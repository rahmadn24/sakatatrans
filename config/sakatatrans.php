<?php

return [
    'cars' => [
        'source'      => env('APP_URL'). 'resources/cars/',
        'upload_path' => 'resources/cars'
    ],

    'profile' => [
        'source'      => env('APP_URL'). 'resources/profile/',
        'upload_path' => 'resources/profile'
    ],
];
