<?php

namespace App\Models;
use App\Models\Cars;
use Illuminate\Database\Eloquent\Model;


class Prices extends Model
{
    protected $fillable = [
        'price',
        'cars_id',
        'level',
    ];

    public function Cars(){
        return $this->belongsTo(Cars::class, 'id');
    }
}
