<?php

namespace App\Models;
use App\Models\Prices;

use Illuminate\Database\Eloquent\Model;


class Cars extends Model
{
    protected $fillable = [
        'name_cars',
        'image',
        'maximum_payload',
        'cubic',
        'width',
        'height',
    ];

    public function Prices(){
        return $this->belongsTo(Prices::class, 'id', 'cars_id');
    }
}
