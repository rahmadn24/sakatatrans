<?php

namespace App\Http\Requests\Cars;

use Illuminate\Foundation\Http\FormRequest;

class CarsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'           => 'required|max:2000',
            'name_cars'       => 'required',
            'maximum_payload' => 'required',
            'cubic'           => 'required',
            'width'           => 'required',
            'height'          => 'required',

        ];
    }

    public function messages()
    {
        return [
            'image.required' => 'Image field is required',
            'image.max' => 'Image above 2MB cannot be uploaded'
        ];
    }
}
