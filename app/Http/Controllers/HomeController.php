<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;
use App\Models\Prices;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Cars\CarsRequest;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->carsModel = new Cars;
        $this->priceModel = new Prices;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $cars = $this->carsModel->get();
            return Datatables::of($cars)
                ->addIndexColumn()
                ->addColumn('action',function($cars){
                    return "
                    <button type='button' name='edit' carId='".$cars->id."' class='edit btn round btn-warning btn-sm'><i class='fas fa-edit'></i></button>
                    <button type='button' href='" . route('cars::delete', ['carId' => $cars->id]) . "' class='btn btn-danger round btn-sm' onclick='deleteHandler(this)'>
                        <i class='fas fa-trash'></i>
                    </button>
                    ";
                })
                ->editColumn('image', function ($cars) {
                    return "
                        <img src='" . config('sakatatrans.cars.source') . '/' . $cars->image . "' height='30'>
                        ";
                })

                ->editColumn('maximum_payload', function ($cars) {
                    return $cars->maximum_payload . ' Kg';
                })

                ->editColumn('cubic', function ($cars) {
                    return $cars->cubic . ' m³';
                })

                ->editColumn('width', function ($cars) {
                    return $cars->width . ' cm';
                })

                ->editColumn('height', function ($cars) {
                    return $cars->height . ' cm';
                })

                ->rawColumns(['action', 'image', 'maximum_payload', 'cubic', 'width', 'height'])
                ->make(true);
        }
        return view('admin/home');
    }

    public function store(CarsRequest $request)
    {
        try {
            if($request->image == null &&  $request->copyImage == null){
                return [
                    'status' => 'error',
                    'message' => 'Image required'
                ];
            }
            if($request->image != null){
                $icon        = $request->image;
                $icon_name   = $request->company . '_' . date('ymdhis') . '.' . $icon->getClientOriginalExtension();
                $upload_path = config('sakatatrans.cars.upload_path');
            }else{
                $icon        = $request->copyImage;
                $icon_name   = $request->copyImage;
            }

            $form_data = array(
                'name_cars'         => $request->name_cars,
                'image'             => $icon_name,
                'maximum_payload'   => $request->maximum_payload,
                'cubic'             => $request->cubic,
                'width'             => $request->width,
                'height'            => $request->height,
            );

            $data   =  $this->carsModel->create($form_data);

            $icon->move($upload_path, $icon_name);

            if($data){
                return [
                    'status' => 'success',
                    'message' => 'Successfully add cars data'
                ];
            }else{
                return [
                    'status' => 'error',
                    'message' => 'Something wrong when add cars data'
                ];
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function edit(Request $request)
    {
        try{
            if(request()->ajax())
            {
                $data = $this->carsModel->findOrFail($request->carId);
                $data->image_do = config('sakatatrans.cars.source') . '/' . $data->image;
                return response()->json([
                    'data' => $data,
                ]);
            }
        }catch(ClientException $e){
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function update(Request $request)
    {
        try{
            $this->validate($request, [
                'name_cars'       => 'required',
                'maximum_payload' => 'required',
                'cubic'           => 'required',
                'width'           => 'required',
                'height'          => 'required',
            ]);

            $icon        = $request->image;
            $icon_name   = $request->oldImage;

            if ($icon) {
                if($request->file('image')->getSize() > 5120){
                    return [
                        'status' => 'error',
                        'message' => 'Image above 2MB cannot be uploaded'
                    ];
                }
                $icon_name = $request->editCompany . '_' . date('ymdhis') . '.' . $icon->getClientOriginalExtension();
                $upload_path = config('sakatatrans.cars.upload_path');
            }

            $form_data = array(
                'name_cars'         => $request->name_cars,
                'image'             => $icon_name,
                'maximum_payload'   => $request->maximum_payload,
                'cubic'             => $request->cubic,
                'width'             => $request->width,
                'height'            => $request->height,
            );

            $this->carsModel->whereId($request->hidden_id)->update($form_data);
            ($icon) ? $icon->move($upload_path, $icon_name) : '';

            if($form_data) {
                return [
                    'status'  => 'success',
                    'message' => 'Cars data updated successfully'
                ];
            }

            return [
                'status'  => 'error',
                'message' => 'Something wrong when updating cars data'
            ];

        }catch(ClientException $e){
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function delete(Request $request)
    {
        try{
            $carId = $request->carId;
            $data = $this->carsModel->findOrFail($carId)->delete();
            if($data){
                return [
                    'status' => 'success',
                    'message' => 'Cars data deleted successfully'
                ];
            }else{
                return [
                    'status' => 'error',
                    'message' => 'Cars data deleted failed'
                ];
            }
        }catch(ClientException $e){
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function price()
    {
        return view('price');
    }


}
