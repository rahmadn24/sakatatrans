<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;
use App\Models\Prices;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Price\PriceRequest;

class PriceCarsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->carsModel = new Cars;
        $this->priceModel = new Prices;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $cars = $this->carsModel->get();

        if ($request->ajax()) {
            $priceCars = $this->priceModel->get();
            return Datatables::of($priceCars)
                ->addIndexColumn()
                ->addColumn('action',function($priceCars){
                    return "
                    <button type='button' name='edit' priceId='".$priceCars->id."' class='edit btn round btn-warning btn-sm'><i class='fas fa-edit'></i></button>
                    <button type='button' href='" . route('price::delete', ['priceId' => $priceCars->id]) . "' class='btn btn-danger round btn-sm' onclick='deleteHandler(this)'>
                        <i class='fas fa-trash'></i>
                    </button>
                    ";
                })

                ->editColumn('price', function($priceCars){
                    return "Rp". number_format($priceCars->price, 0, ',', '.');
                })

                ->editColumn('cars_id', function ($priceCars) {
                    $carsName = $this->carsModel->with('prices')->where('id', $priceCars->cars_id)->first();
                    $name =  $carsName->name_cars;
                    return $name;
                })

                ->editColumn('level', function ($priceCars) {
                    if($priceCars->level == 10){
                        $level = "Under 10 Km";
                    }elseif($priceCars->level == 20){
                        $level = "Under 20 Km";
                    }elseif ($priceCars->level == 30) {
                       $level = "Under 30 Km";
                    }elseif ($priceCars->level == 40) {
                       $level = "Under 40 Km";
                    }elseif ($priceCars->level == 50) {
                        $level = "Under 50 Km";
                    }elseif ($priceCars->level == 60) {
                        $level = "Under 60 Km";
                    }elseif ($priceCars->level == 70) {
                        $level = "Under 70 Km";
                    }elseif ($priceCars->level == 80) {
                        $level = "Under 80 Km";
                    }elseif ($priceCars->level == 90) {
                        $level = "Under 90 Km";
                    }elseif ($priceCars->level == 100) {
                        $level = "Under 100 Km";
                    }elseif ($priceCars->level == 120) {
                        $level = "Under 120 Km";
                    }elseif ($priceCars->level == 140) {
                        $level = "Under 140 Km";
                    }elseif ($priceCars->level == 160) {
                        $level = "Under 160 Km";
                    }elseif ($priceCars->level == 180) {
                        $level = "Under 180 Km";
                    }elseif ($priceCars->level == 200) {
                        $level = "Under 200 Km";
                    }elseif ($priceCars->level == 220) {
                        $level = "Under 220 Km";
                    }elseif ($priceCars->level == 240) {
                        $level = "Under 240 Km";
                    }elseif ($priceCars->level == 260) {
                        $level = "Under 260 Km";
                    }elseif ($priceCars->level == 280) {
                        $level = "Under 280 Km";
                    }elseif ($priceCars->level == 300) {
                        $level = "Under 300 Km";
                    }elseif ($priceCars->level == 320) {
                        $level = "Under 320 Km";
                    }elseif ($priceCars->level == 340) {
                        $level = "Under 340 Km";
                    }elseif ($priceCars->level == 360) {
                        $level = "Under 360 Km";
                    }elseif ($priceCars->level == 24) {
                        $level = "Under 380 Km";
                    }elseif ($priceCars->level == 400) {
                        $level = "Under 400 Km";
                    }elseif ($priceCars->level == 450) {
                        $level = "Under 450 Km";
                    }elseif ($priceCars->level == 500) {
                        $level = "Under 500 Km";
                    }elseif ($priceCars->level == 550) {
                        $level = "Under 550 Km";
                    }elseif ($priceCars->level == 600) {
                        $level = "Under 600 Km";
                    }elseif ($priceCars->level == 650) {
                        $level = "Under 650 Km";
                    }elseif ($priceCars->level == 700) {
                        $level = "Under 700 Km";
                    }elseif ($priceCars->level == 750) {
                        $level = "Under 750 Km";
                    }elseif ($priceCars->level == 800) {
                        $level = "Under 800 Km";
                    }elseif ($priceCars->level == 850) {
                        $level = "Under 850 Km";
                    }elseif ($priceCars->level == 900) {
                        $level = "Under 900 Km";
                    }elseif ($priceCars->level == 950) {
                        $level = "Under 950 Km";
                    }elseif ($priceCars->level == 1000) {
                        $level = "Under 1000 Km";
                    }else{
                        $level = "1000 Km Up";
                    }
                    return $level;
                })

                ->rawColumns(['action', 'cars_id', 'level'])
                ->make(true);
        }
        return view('admin/price-cars', ['cars' => $cars]);
    }

    public function store(PriceRequest $request)
    {
        try {
            $form_data = array(
                'price'     => str_replace(".", "", $request->price),
                'cars_id'   => $request->cars,
                'level'     => $request->level,
            );

            $data   =  $this->priceModel->create($form_data);

            if($data){
                return [
                    'status' => 'success',
                    'message' => 'Successfully add price data'
                ];
            }else{
                return [
                    'status' => 'error',
                    'message' => 'Something wrong when add price data'
                ];
            }
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function edit(Request $request)
    {
        try{
            if(request()->ajax())
            {
                $data = $this->priceModel->findOrFail($request->priceId);
                $idrPrice = number_format($data->price, 0, ',', '.');
                return response()->json([
                    'data' => $data,
                    'idrPrice' => $idrPrice,
                ]);
            }
        }catch(ClientException $e){
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function update(PriceRequest $request)
    {
        try{
            $form_data = array(
                'price'     => str_replace(".", "", $request->price),
                'cars_id'   => $request->cars,
                'level'     => $request->level,
            );

            $this->priceModel->whereId($request->hidden_id)->update($form_data);

            if($form_data) {
                return [
                    'status'  => 'success',
                    'message' => 'Price data updated successfully'
                ];
            }

            return [
                'status'  => 'error',
                'message' => 'Something wrong when updating price data'
            ];

        }catch(ClientException $e){
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function delete(Request $request)
    {
        try{
            $priceId = $request->priceId;
            $data = $this->priceModel->findOrFail($priceId)->delete();
            if($data){
                return [
                    'status' => 'success',
                    'message' => 'Price data deleted successfully'
                ];
            }else{
                return [
                    'status' => 'error',
                    'message' => 'Price data deleted failed'
                ];
            }
        }catch(ClientException $e){
            return response()->json([
                'status' => 400,
                'data' => null,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function price(){
        $datas = $this->carsModel->with('prices')->get();

        return view('price',  ['cars' => $datas]);
    }
}
