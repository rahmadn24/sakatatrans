<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Cars;
use App\Models\Prices;
use Yajra\DataTables\DataTables;

class CarsController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->carsModel = new Cars;
        $this->priceModel = new Prices;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getCars()
    {
       $cars = $this->carsModel->with('prices')->get();
       $data = [];
       foreach($cars as $key => $val){
            $data[] = [
                'id'    => $val->id,
                'cars_name' => $val->name_cars,
                'image' =>  config('sakatatrans.cars.source') . '/' . $val->image,
                'maximum_payload' => $val->maximum_payload,
                'cubic' => $val->cubic,
                'width' => $val->width,
                'height' => $val->height,
                'price' => $val->prices->price,
            ];
       }

       if($data){
            return response()->json([
                'data' => $data,
                'status' => 200,
            ]);
       }else{
            return response()->json([
                'data' => null,
                'status' => 200,
            ]);
       }
    }

    public function getDetail(Request $request)
    {
        $data = (json_decode($request->json));
        $car = $this->carsModel->where('id', $data->id)->first();

        $distance = (float) $data->distance;

        $price = $price = $this->priceModel->where('cars_id', $data->id)->get();
        foreach($price as $key => $val){
            if($val->level <= $distance){
                $totalPrice = $distance * $val->price;
            }else{
                $totalPrice = 0;
            };
        }

        $data =[
             'cars' => $this->carsModel->where('id', $data->id)->first(),
             'image' => config('sakatatrans.cars.source') . '/' . $car->image,
             'totalPrice' => $totalPrice,
             'distance' => $distance,
        ];

        if($data){
            return response()->json([
                'data' => $data,
                'status' => 200,
            ]);
        }else{
            return response()->json([
                'data' => null,
                'status' => 200,
            ]);
        }
    }

}
