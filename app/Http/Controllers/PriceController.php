<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cars;
use App\Models\Prices;
use Yajra\DataTables\DataTables;
use App\Http\Requests\Price\PriceRequest;

class PriceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->carsModel = new Cars;
        $this->priceModel = new Prices;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function price(){
        $datas = $this->carsModel->with('prices')->get();

        return view('price',  ['cars' => $datas]);
    }
}
