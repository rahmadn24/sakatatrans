<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

#Routes for Cars
// $router->group(['as' => 'cars-api::'], function() use ($router) {
//     $router->get('/get-cars', ['as' => 'management-sakata', 'uses' => 'CarsController@getCars']);
// });

Route::get('/getCars', ['as' => 'get-cars', 'uses' => 'Api\CarsController@getCars']);
Route::post('/getDetail', ['as' => 'get-detail', 'uses' => 'Api\CarsController@getDetail']);