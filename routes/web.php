<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/price', ['as' => 'price', 'uses' => 'PriceController@price']);

// Route::get('/price', function () {
//     return view('price');
// });

Auth::routes();

#Routes for Cars
$router->group(['as' => 'cars::', 'middleware' => ['auth']], function() use ($router) {
    $router->get('/managementSakata', ['as' => 'management-sakata', 'uses' => 'HomeController@index']);
    $router->post('/cars-store', ['as' => 'store', 'uses' => 'HomeController@store']);
    $router->get('/cars/{carId}/edit', ['as' => 'car-edit', 'uses' => 'HomeController@edit']);
    $router->post('cars/update', ['as' => 'cars-update', 'uses' => 'HomeController@update']);
    $router->get('{carId}/delete', ['as' => 'delete', 'uses' => 'HomeController@delete']);
});

#Routes for Price cars
$router->group(['as' => 'price::', 'middleware' => ['auth']], function() use ($router) {
    $router->get('/managementSakata-price', ['as' => 'management-sakata', 'uses' => 'PriceCarsController@index']);
    $router->post('/price-store', ['as' => 'store', 'uses' => 'PriceCarsController@store']);
    $router->get('/price/{priceId}/edit', ['as' => 'car-edit', 'uses' => 'PriceCarsController@edit']);
    $router->post('price/update', ['as' => 'price-update', 'uses' => 'PriceCarsController@update']);
    $router->get('managementSakata-price/{priceId}/delete', ['as' => 'delete', 'uses' => 'PriceCarsController@delete']);
});